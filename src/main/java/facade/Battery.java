package facade;

public class Battery {
    double getPowerInPercentage() {
        return Math.random() * 100;
    }

    boolean isBroken() {
        return false;
    }
}
