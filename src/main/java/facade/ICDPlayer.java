package facade;

public interface ICDPlayer {
    boolean turnOn();
    boolean turnOff();
    boolean nextSong();
}
