package facade;

public class Button {
    void press() {
        System.out.println("Press button");
    }
}
