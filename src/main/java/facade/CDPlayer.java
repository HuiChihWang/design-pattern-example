package facade;

public class CDPlayer implements ICDPlayer {
    private final Switch s;
    private final Button button;
    private final Battery battery;

    public CDPlayer(Switch s, Button button, Battery battery) {
        this.s = s;
        this.button = button;
        this.battery = battery;
    }

    @Override
    public boolean turnOn() {
        if (battery.isBroken() || battery.getPowerInPercentage() < 1) {
            return false;
        }
        s.toggle();
        return true;
    }

    @Override
    public boolean turnOff() {
        if (!s.isOn()) {
            return false;
        }
        s.toggle();
        return true;
    }

    @Override
    public boolean nextSong() {
        button.press();
        return true;
    }
}
