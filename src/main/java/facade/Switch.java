package facade;

public class Switch {
    private boolean isOn = false;
    void toggle() {
        isOn = !isOn;
    }

    boolean isOn() {
        return isOn;
    }
}
