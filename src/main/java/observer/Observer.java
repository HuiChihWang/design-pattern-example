package observer;

public interface Observer<T> {
    void onNotified(T updatedData);
}
