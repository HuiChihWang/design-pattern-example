package observer;

public class WeatherStation extends Observable<WeatherData> {

    private int humidity = 0;
    private int temperature = 0;
    private int windSpeed = 0;

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
        notifyObservers(getWeatherData());
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
        notifyObservers(getWeatherData());
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(int windSpeed) {
        this.windSpeed = windSpeed;
        notifyObservers(getWeatherData());
    }

    public WeatherData getWeatherData() {
        return new WeatherData(humidity, windSpeed, temperature);
    }
}
