package observer;

public class WebDisplay implements Observer<WeatherData> {
    private WeatherData weatherData;

//    private SystemData systemData;


    @Override
    public void onNotified(WeatherData updatedData) {
        weatherData = updatedData;

        System.out.println("Update data on WebDisplay");
        System.out.println(weatherData.toString());
        System.out.println();
    }
}
