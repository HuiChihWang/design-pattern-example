package observer;

public class PhoneDisplay implements Observer<WeatherData> {
    private WeatherData weatherData;


    @Override
    public void onNotified(WeatherData updatedData) {
        weatherData = updatedData;

        System.out.println("Update data on PhoneDisplay");
        System.out.println(weatherData.toString());
        System.out.println();
    }
}
