package observer;

public record WeatherData(
        int humidity,
        int windSpeed,
        int temperature
) {
}
