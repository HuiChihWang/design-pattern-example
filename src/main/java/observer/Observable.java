package observer;

import java.util.HashSet;
import java.util.Set;

public abstract class Observable<T> {
    final Set<Observer<T>> observers = new HashSet<>();

    void addObserver(Observer<T> observer) {
        observers.add(observer);
    }

    void removeObserver(Observer<T> observer) {
        observers.remove(observer);
    }

    void notifyObservers(T updatedData) {
        for (var observer: observers) {
            observer.onNotified(updatedData);
        }
    }
}
