package observer;

public class TestObserver {
    public static void main(String[] args) {
        WeatherStation station = new WeatherStation();

        PhoneDisplay phoneDisplay = new PhoneDisplay();
        WebDisplay webDisplay = new WebDisplay();

        System.out.println("//////////////Test 1////////////////");
        station.addObserver(phoneDisplay);
        station.addObserver(webDisplay);

        station.setTemperature(30);
        station.setWindSpeed(10);

        System.out.println("////////////////////////////////////");

        System.out.println("//////////////Test 2////////////////");
        station.removeObserver(phoneDisplay);
        station.addObserver(webDisplay);

        station.setTemperature(-5);
        station.setWindSpeed(2);

        System.out.println("////////////////////////////////////");
    }
}
