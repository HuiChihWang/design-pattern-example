package template;

public abstract class EmailTemplate {
    public abstract String getSender();

    public abstract String getRecipient();

    public abstract String getSubject();

    public abstract String getContent();

    public final String getEmailLog() {
        return "Email sent from: " + getSender() + " to: " + getRecipient() + " with subject: " + getSubject() + " and content: " + getContent();
    }
}
