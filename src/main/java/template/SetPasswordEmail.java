package template;

public class SetPasswordEmail extends EmailTemplate {
    @Override
    public String getSender() {
        return "market@gmail.com";
    }

    @Override
    public String getRecipient() {
        return "taya87136@gmail.com";
    }

    @Override
    public String getSubject() {
        return "Set Password";
    }

    @Override
    public String getContent() {
        return "Please set your password by clicking this link: ...";
    }
}
