package decorator;

import decorator.algorithm.Algorithm;
import decorator.algorithm.SimpleAlgorithm;

public class TestDecorator {
    public static void main(String[] args) {
        Algorithm algorithm = new SimpleAlgorithm();
        System.out.println(algorithm.calculate());
    }
}
