package decorator.algorithm;

public class BaseAlgorithm implements Algorithm {
    private final double initialValue;

    public BaseAlgorithm(double initialValue) {
        this.initialValue = initialValue;
    }

    @Override
    public double calculate() {
        return initialValue;
    }
}
