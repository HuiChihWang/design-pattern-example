package decorator.algorithm;

public interface Algorithm {
    double calculate();
}
