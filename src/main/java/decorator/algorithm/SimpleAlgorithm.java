package decorator.algorithm;

import decorator.arithmetic.*;

public class SimpleAlgorithm implements Algorithm {
    private final Algorithm algorithm;

    // ( (1 + 3) * 4 - 6 ) / 2 = 5
    public SimpleAlgorithm() {
        this.algorithm = initializeAlgorithm();
    }

    private Algorithm initializeAlgorithm() {
        Algorithm newAlgorithm = new BaseAlgorithm(1.);
        newAlgorithm = new AddOperator(newAlgorithm, 3.);
        newAlgorithm = new MultiplyOperator(newAlgorithm, 4.);
        newAlgorithm = new SubtractOperator(newAlgorithm, 6.);
        newAlgorithm = new DivideOperator(newAlgorithm, 2.);
        return newAlgorithm;
    }

    @Override
    public double calculate() {
        return algorithm.calculate();
    }
}
