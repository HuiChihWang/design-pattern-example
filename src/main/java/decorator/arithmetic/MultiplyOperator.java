package decorator.arithmetic;

import decorator.algorithm.Algorithm;

public class MultiplyOperator extends ArithmeticOperator {
    public MultiplyOperator(Algorithm algorithm, double postNumber) {
        super(algorithm, postNumber);
    }

    @Override
    public double calculate() {
        return algorithm.calculate() * postNumber;
    }
}
