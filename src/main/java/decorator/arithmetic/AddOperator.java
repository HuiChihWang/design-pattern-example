package decorator.arithmetic;

import decorator.algorithm.Algorithm;

public class AddOperator extends ArithmeticOperator {
    public AddOperator(Algorithm algorithm, double postNumber) {
        super(algorithm, postNumber);
    }

    @Override
    public double calculate() {
        return this.algorithm.calculate() + this.postNumber;
    }
}
