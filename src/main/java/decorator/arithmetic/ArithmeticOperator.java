package decorator.arithmetic;

import decorator.algorithm.Algorithm;

public abstract class ArithmeticOperator implements Algorithm {
    protected double postNumber = 1.;
    protected Algorithm algorithm;

    protected ArithmeticOperator(Algorithm algorithm, double postNumber) {
        this.algorithm = algorithm;
        this.postNumber = postNumber;
    }
}
