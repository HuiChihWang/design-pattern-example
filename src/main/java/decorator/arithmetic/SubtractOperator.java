package decorator.arithmetic;

import decorator.algorithm.Algorithm;

public class SubtractOperator extends ArithmeticOperator {
    public SubtractOperator(Algorithm algorithm, double postNumber) {
        super(algorithm, postNumber);
    }

    @Override
    public double calculate() {
        return algorithm.calculate() - postNumber;
    }
}
