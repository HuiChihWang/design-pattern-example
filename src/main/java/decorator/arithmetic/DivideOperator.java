package decorator.arithmetic;

import decorator.algorithm.Algorithm;

public class DivideOperator extends ArithmeticOperator {
    public DivideOperator(Algorithm algorithm, double postNumber) {
        super(algorithm, postNumber);
    }

    @Override
    public double calculate() {
        if (postNumber == 0) {
            throw new IllegalArgumentException("Divisor should not be 0");
        }
        return algorithm.calculate() / postNumber;
    }
}
