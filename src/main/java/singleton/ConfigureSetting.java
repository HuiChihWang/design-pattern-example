package singleton;

public class ConfigureSetting {
    public static ConfigureSetting getInstance() {
        if (instance == null) {
            instance = new ConfigureSetting();
        }
        return instance;
    }
    private static ConfigureSetting instance;
    private ConfigureSetting() {}

    public void setUp() {}

    public static void main(String[] args) {
        ConfigureSetting setting = ConfigureSetting.getInstance();
        setting.setUp();
    }
}
