package singleton;

public enum SystemSetting {
    INSTANCE;
    void setSettingByKey(String key, Object setting) {}

    public static void main(String[] args) {
        SystemSetting.INSTANCE.setSettingByKey("display", new Object());
    }
}
