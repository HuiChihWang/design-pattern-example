package adapter;

public class Client {
    public static void main(String[] args) {
        IOldSystem oldS = new OldSystemImp();
        String dataWithoutDate = oldS.getData();
        System.out.println(dataWithoutDate);

        ISystem newS = new OldSystemAdapter(oldS);
        String dataWithDate = newS.getDataWithDate();
        System.out.println(dataWithDate);

        ISystem newS2 = new OldSystemClassAdapter();
        System.out.println(newS2.getDataWithDate());
        IOldSystem newS3 = (IOldSystem) newS2;
        System.out.println(newS3.getData());
    }
}
