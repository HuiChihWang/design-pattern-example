package adapter;

public interface ISystem {
    String getDataWithDate();
}
