package adapter;

import java.time.LocalTime;

public class OldSystemClassAdapter extends OldSystemImp implements ISystem{
    @Override
    public String getDataWithDate() {
        return getData() + " " + LocalTime.now();
    }
}
