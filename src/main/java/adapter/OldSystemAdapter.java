package adapter;

import java.time.LocalTime;

public class OldSystemAdapter implements ISystem {
    private final IOldSystem IOldSystem;
    public OldSystemAdapter(IOldSystem IOldSystem) {
        this.IOldSystem = IOldSystem;
    }

    @Override
    public String getDataWithDate() {
        return IOldSystem.getData() + " " + LocalTime.now();
    }
}
