package adapter;

public class OldSystemImp implements IOldSystem {

    @Override
    public String getData() {
        return "old data without date";
    }
}
