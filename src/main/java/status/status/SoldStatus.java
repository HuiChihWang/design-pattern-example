package status.status;

import status.Beverage;
import status.BeverageVendingMachine;

public class SoldStatus extends MachineStatus {
    public SoldStatus(BeverageVendingMachine machine) {
        super(machine);
    }

    @Override
    public void insertCoins(double amount) {
        throw new RuntimeException("Sold out");
    }

    @Override
    public Beverage selectItem(int itemId) {
        throw new RuntimeException("Sold out");
    }

    @Override
    public double refund() {
        double currentAmount = machine.getMoneyAmount();
        machine.setMoneyAmount(0);

        if (machine.isEmpty()) {
            machine.setStatus(machine.soldOutStatus);
        } else {
            machine.setStatus(machine.readyStatus);
        }

        return currentAmount;
    }

    @Override
    public void refill(int itemId, int number) {
        throw new RuntimeException("Sold out");
    }
}
