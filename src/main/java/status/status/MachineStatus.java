package status.status;

import status.Beverage;
import status.BeverageVendingMachine;

public abstract class MachineStatus {
    protected final BeverageVendingMachine machine;
    MachineStatus(BeverageVendingMachine machine) {
        this.machine = machine;
    }

    public abstract void insertCoins(double amount);
    public abstract Beverage selectItem(int itemId);
    public abstract double refund();
    public abstract void refill(int itemId, int number);

}
