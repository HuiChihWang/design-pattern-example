package status.status;

import status.Beverage;
import status.BeverageVendingMachine;

public class SoldOutStatus extends MachineStatus {
    public SoldOutStatus(BeverageVendingMachine machine) {
        super(machine);
    }

    @Override
    public void insertCoins(double amount) {
        throw new RuntimeException("Sold out");
    }

    @Override
    public Beverage selectItem(int itemId) {
        throw new RuntimeException("Sold out");
    }

    @Override
    public double refund() {
        throw new RuntimeException("No money to refund");
    }

    @Override
    public void refill(int itemId, int number) {
        this.machine.addItems(itemId, number);
        this.machine.setStatus(this.machine.readyStatus);
    }
}
