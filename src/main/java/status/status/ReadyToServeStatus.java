package status.status;

import status.Beverage;
import status.BeverageVendingMachine;

public class ReadyToServeStatus extends MachineStatus {

    public ReadyToServeStatus(BeverageVendingMachine machine) {
        super(machine);
    }

    @Override
    public void insertCoins(double amount) {
        machine.addMoney(amount);
        machine.setStatus(machine.haveMoneyStatus);
    }

    @Override
    public Beverage selectItem(int itemId) {
        throw new RuntimeException("Please insert money first");
    }

    @Override
    public double refund() {
        throw new RuntimeException("No money to refund");
    }

    @Override
    public void refill(int itemId, int number) {
        machine.addItems(itemId, number);
    }
}
