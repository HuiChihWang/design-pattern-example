package status.status;


import status.Beverage;
import status.BeverageVendingMachine;

public class HaveMoneyStatus extends MachineStatus {
    public HaveMoneyStatus(BeverageVendingMachine machine) {
        super(machine);
    }

    @Override
    public void insertCoins(double amount) {
        machine.addMoney(amount);
    }

    @Override
    public Beverage selectItem(int itemId) {
        Beverage selectedItem = machine.getItems().get(itemId);
        machine.pollItem(itemId);
        machine.setStatus(machine.soldStatus);
        return selectedItem;
    }

    @Override
    public double refund() {
        double currentAmount = machine.getMoneyAmount();
        machine.setMoneyAmount(0);
        machine.setStatus(machine.readyStatus);
        return currentAmount;
    }

    @Override
    public void refill(int itemId, int number) {
        throw new RuntimeException("you cannot fill item when there is money");
    }
}
