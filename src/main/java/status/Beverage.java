package status;


public class Beverage {
    public Beverage(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String name;

    public double price;
}
