package status;

import status.status.*;

import java.util.HashMap;
import java.util.Map;

public class BeverageVendingMachine {
    public MachineStatus soldOutStatus;
    public MachineStatus readyStatus;
    public MachineStatus haveMoneyStatus;

    public MachineStatus soldStatus;
    private MachineStatus status;

    private double moneyAmount = 0;

    Map<Integer, Beverage> beverageList = new HashMap<>();
    Map<Integer, Integer> quantity = new HashMap<>();


    public BeverageVendingMachine() {
        soldOutStatus = new SoldOutStatus(this);
        readyStatus = new ReadyToServeStatus(this);
        haveMoneyStatus = new HaveMoneyStatus(this);
        soldStatus = new SoldStatus(this);
        status = soldOutStatus;
    }

    public void insertCoins(double amount) {
        if (amount <= 0) {
            return;
        }
        status.insertCoins(amount);
    }

    public Beverage selectItem(Integer itemId) {
        if (!beverageList.containsKey(itemId)) {
            throw new RuntimeException("No such item");
        }

        if (!quantity.containsKey(itemId) || quantity.get(itemId) <= 0) {
            throw new RuntimeException("Sold out");
        }

        if (beverageList.get(itemId).price < moneyAmount) {
            throw new RuntimeException("Insufficient money");
        }

        return status.selectItem(itemId);
    }

    public double refund() {
        return status.refund();
    }

    public void refill(int itemId, int number) {
        status.refill(itemId, number);
    }

    public Map<Integer, Beverage> getItems() {
        return beverageList;
    }

    public double getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(double newAmount) {
        this.moneyAmount = newAmount;
    }

    public void setStatus(MachineStatus status) {
        this.status = status;
    }

    public void addItems(int itemId, int number) {
        if (!beverageList.containsKey(itemId)) {
            return;
        }

        int currentNum = quantity.getOrDefault(itemId, 0);
        quantity.put(itemId, currentNum + number);
    }

    public void pollItem(int itemId) {
        if (!beverageList.containsKey(itemId)) {
            return;
        }

        int currentNum = quantity.getOrDefault(itemId, 0);
        if (currentNum <= 0) {
            return;
        }

        quantity.put(itemId, currentNum - 1);
        if (quantity.get(itemId) == 0) {
            quantity.remove(itemId);
        }
    }

    public void addMoney(double amount) {
        this.moneyAmount += amount;
    }

    public boolean isEmpty() {
        return quantity.isEmpty();
    }
}
