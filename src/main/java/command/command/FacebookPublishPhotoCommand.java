package command.command;

import command.sdk.FacebookSDK;

public class FacebookPublishPhotoCommand implements Command {
    private final FacebookSDK facebookSDK = new FacebookSDK();

    private String photoName;

    void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    @Override
    public void execute() {
        facebookSDK.getToken("username", "password");
        facebookSDK.publishPhoto(photoName);
    }
}
