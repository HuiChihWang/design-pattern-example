package command.command;

import command.sdk.InstagramSDK;

public class InstagramShareStoryCommand implements Command {
    private final InstagramSDK instagramSDK = new InstagramSDK();

    private String storyContent;

    public void setStoryContent(String storyContent) {
        this.storyContent = storyContent;
    }

    @Override
    public void execute() {
        instagramSDK.getToken("username", "password");
        instagramSDK.shareStory(storyContent);
    }
}
