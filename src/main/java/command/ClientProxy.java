package command;

import command.command.Command;

import java.util.ArrayList;
import java.util.List;

public class ClientProxy {
    List<Command> commands = new ArrayList<>();

    public void setCommands(List<Command> commands) {
        this.commands = commands;
    }

    public void executeSocialActivities() {
        for (Command command: commands) {
            command.execute();
        }
    }
}
