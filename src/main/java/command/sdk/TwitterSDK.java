package command.sdk;

public class TwitterSDK {
    public String getToken(String username, String password) {
        System.out.println("Logging in to Twitter with username " + username + " and password " + password);
        return "token";
    }

    public void reTweet(String tweetName) {
        System.out.println("Retweeting tweet " + tweetName + " on Twitter");
    }

    public void postTweet(String tweetName) {
        System.out.println("Posting tweet " + tweetName + " on Twitter");
    }
}
