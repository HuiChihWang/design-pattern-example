package command.sdk;

public class FacebookSDK {
    public void getToken(String username, String password) {
        System.out.println("Logging in to Facebook with username " + username + " and password " + password);
    }

    public void joinGroup(String groupName) {
        System.out.println("Joining group " + groupName + " on Facebook");
    }

    public void sharePhoto(String photoName) {
        System.out.println("Sharing photo " + photoName + " on Facebook");
    }

    public void publishPhoto(String photoName) {
        System.out.println("Publishing post " + photoName + " on Facebook");
    }

    public void updateProfile(String profileName) {
        System.out.println("Updating profile " + profileName + " on Facebook");
    }

}
