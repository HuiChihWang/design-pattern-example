package command.sdk;

public class InstagramSDK {
    public String getToken(String username, String password) {
        System.out.println("Logging in to Instagram with username " + username + " and password " + password);
        return "token";
    }

    public void shareVideo(String videoName) {
        System.out.println("Sharing video " + videoName + " on Instagram");
    }

    public void sharePhoto(String photoName) {
        System.out.println("Sharing photo " + photoName + " on Instagram");
    }

    public void shareStory(String storyName) {
        System.out.println("Sharing story " + storyName + " on Instagram");
    }
}
