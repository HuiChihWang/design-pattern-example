package factory.vendor;

import factory.computer.Computer;
import factory.componentFactory.ComputerComponentFactory;
import factory.computer.ComputerType;

//Simple Factory
public interface Vendor {
    Computer provideComputer(ComputerType type);
}
