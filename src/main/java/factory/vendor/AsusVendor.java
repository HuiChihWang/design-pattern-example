package factory.vendor;

import factory.componentFactory.AsusComponentFactory;
import factory.componentFactory.GigaByteComponentFactory;
import factory.computer.Computer;
import factory.computer.ComputerType;
import factory.computer.Desktop;
import factory.computer.Notebook;

public class AsusVendor implements Vendor {
    @Override
    public Computer provideComputer(ComputerType type) {
        switch (type) {
            case DESKTOP:
                return new Desktop(AsusComponentFactory.INSTANCE);
            case NOTEBOOK:
                return new Notebook(AsusComponentFactory.INSTANCE);
            default:
                return null;
        }
    }
}
