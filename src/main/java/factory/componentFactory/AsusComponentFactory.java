package factory.componentFactory;

import factory.component.cpu.CPU;
import factory.component.cpu.IntelCPU;
import factory.component.disk.Disk;
import factory.component.disk.SSD;
import factory.component.memory.KingstonDDR;
import factory.component.memory.Memory;
import factory.component.motherboard.ASUSMotherBoard;
import factory.component.motherboard.MotherBoard;

public enum AsusComponentFactory implements ComputerComponentFactory {
    INSTANCE;

    @Override
    public MotherBoard createMotherBoard() {
        return new ASUSMotherBoard();
    }

    @Override
    public CPU createCPU() {
        return new IntelCPU();
    }

    @Override
    public Disk createDisk() {
        return new SSD();
    }

    @Override
    public Memory createMemory() {
        return new KingstonDDR();
    }
}
