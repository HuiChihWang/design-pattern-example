package factory.componentFactory;

import factory.component.cpu.CPU;
import factory.component.cpu.IntelCPU;
import factory.component.disk.Disk;
import factory.component.disk.HDD;
import factory.component.memory.Memory;
import factory.component.memory.MicroDDR;
import factory.component.motherboard.ASUSMotherBoard;
import factory.component.motherboard.MotherBoard;

public enum GigaByteComponentFactory implements ComputerComponentFactory {
    INSTANCE;

    @Override
    public MotherBoard createMotherBoard() {
        return new ASUSMotherBoard();
    }

    @Override
    public CPU createCPU() {
        return new IntelCPU();
    }

    @Override
    public Disk createDisk() {
        return new HDD();
    }

    @Override
    public Memory createMemory() {
        return new MicroDDR();
    }
}
