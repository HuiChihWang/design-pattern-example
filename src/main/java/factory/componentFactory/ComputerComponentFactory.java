package factory.componentFactory;

import factory.component.cpu.CPU;
import factory.component.disk.Disk;
import factory.component.memory.Memory;
import factory.component.motherboard.MotherBoard;

//Abstract Factory

public interface ComputerComponentFactory {
    MotherBoard createMotherBoard();
    CPU createCPU();

    Disk createDisk();
    Memory createMemory();
}
