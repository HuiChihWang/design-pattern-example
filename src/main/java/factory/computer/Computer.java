package factory.computer;

import factory.component.cpu.CPU;
import factory.component.disk.Disk;
import factory.component.memory.Memory;
import factory.component.motherboard.MotherBoard;
import factory.componentFactory.ComputerComponentFactory;

public abstract class Computer {
    private ComputerComponentFactory factory;

    private CPU cpu;
    private MotherBoard board;

    private Memory memory;

    private Disk hdd;

    public Computer(ComputerComponentFactory factory) {
        this.factory = factory;
    }

    public void buildUp() {
        cpu = factory.createCPU();
        memory = factory.createMemory();
        hdd = factory.createDisk();
        board = factory.createMotherBoard();

        buildPipeLine();
    }

    abstract protected void buildPipeLine();
}
