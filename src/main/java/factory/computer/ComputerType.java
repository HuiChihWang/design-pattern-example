package factory.computer;

public enum ComputerType {
    DESKTOP,
    NOTEBOOK,
}
