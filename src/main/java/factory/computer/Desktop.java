package factory.computer;

import factory.componentFactory.ComputerComponentFactory;

public class Desktop extends Computer {
    public Desktop(ComputerComponentFactory factory) {
        super(factory);
    }

    @Override
    protected void buildPipeLine() {
        System.out.println("Build Desktop...");
    }
}
