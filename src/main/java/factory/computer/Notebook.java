package factory.computer;

import factory.componentFactory.ComputerComponentFactory;

public class Notebook extends Computer {
    public Notebook(ComputerComponentFactory factory) {
        super(factory);
    }

    @Override
    protected void buildPipeLine() {
        System.out.println("Build Notebook...");
    }
}
