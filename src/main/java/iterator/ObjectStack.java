package iterator;

import java.util.Iterator;

public class ObjectStack<T> implements Iterable<T> {
    private final T[] items;
    private int count;

    public ObjectStack(int capacity) {
        items = (T[]) new Object[capacity];
        count = 0;
    }

    public void add(T item) {
        items[count++] = item;
        count++;
    }

    public T pop() {
        if (count == 0) {
            throw new RuntimeException("List is empty");
        }
        return items[--count];
    }

    public T peek() {
        if (count == 0) {
            throw new RuntimeException("List is empty");
        }
        return items[count - 1];
    }

    public int size() {
        return count;
    }

    public boolean isFull() {
        return count == items.length;
    }

    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public Iterator<T> iterator() {
        return new ObjectStackIterator();
    }

    private class ObjectStackIterator implements Iterator<T> {
        private int index = count - 1;

        @Override
        public boolean hasNext() {
            return index >= 0;
        }

        @Override
        public T next() {
            return items[index--];
        }
    }
}
