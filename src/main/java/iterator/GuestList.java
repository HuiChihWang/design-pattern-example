package iterator;

import java.util.Iterator;
import java.util.Set;

public class GuestList implements Iterable<Guest> {
    Set<Guest> guests;

    public GuestList(Set<Guest> guests) {
        this.guests = guests;
    }

    public void addGuest(Guest guest) {
        guests.add(guest);
    }

    public void removeGuest(Guest guest) {
        guests.remove(guest);
    }

    @Override
    public Iterator<Guest> iterator() {
        return guests.iterator();
    }
}
