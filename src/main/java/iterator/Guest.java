package iterator;

public record Guest(
        String name,
        String email,
        String phone
) {
}
