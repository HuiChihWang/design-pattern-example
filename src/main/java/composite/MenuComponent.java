package composite;

import java.util.function.Consumer;

public abstract class MenuComponent {

    public void add(MenuComponent menuComponent) {
        throw new UnsupportedOperationException();
    }

    public void remove(MenuComponent menuComponent) {
        throw new UnsupportedOperationException();
    }

    public MenuComponent getChild(int i) {
        throw new UnsupportedOperationException();
    }


    // operations
    public void processMenu(Consumer<MenuComponent> consumer) {
        throw new UnsupportedOperationException();
    }
}
