package composite;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class MenuComposite extends MenuComponent {
    private final List<MenuComponent> subItems = new ArrayList<>();
    private final String name;

    public MenuComposite(String name) {
        this.name = name;
    }

    public void add(MenuComponent menuComponent) {
        subItems.add(menuComponent);
    }

    @Override
    public void remove(MenuComponent menuComponent) {
        subItems.remove(menuComponent);
    }

    @Override
    public MenuComponent getChild(int i) {
        return subItems.get(i);
    }

    @Override
    public void processMenu(Consumer<MenuComponent> consumer) {
        consumer.accept(this);
        for (MenuComponent subItem : subItems) {
            subItem.processMenu(consumer);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("MenuComposite{" + "name='" + name + '\'' + '}');
        for (MenuComponent subItem : subItems) {
            sb.append("\n").append(subItem);
        }
        return sb.toString();
    }
}
