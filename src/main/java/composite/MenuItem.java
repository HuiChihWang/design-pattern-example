package composite;

import java.util.function.Consumer;

public class MenuItem extends MenuComponent {
    private final String name;
    private final String description;
    private final String price;

    public MenuItem(String name, String description, String price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    @Override
    public void processMenu(Consumer<MenuComponent> consumer) {
        consumer.accept(this);
    }


    @Override
    public String toString() {
        return "MenuItem{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
